import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  value1: number;
  value2: number;
  result: number;
  title = 'SimpleCalculator';

  add() {
    this.result = (this.value1 ?? 0) + (this.value2 ?? 0);
  }
}
