import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
  tick,
} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule],
      providers: [{ provide: ComponentFixtureAutoDetect, useValue: true }],
      declarations: [AppComponent],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'SimpleCalculator'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('SimpleCalculator');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h3').textContent).toContain(
      'SIMPLE CALCULATOR'
    );
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);

    const hostElement = fixture.nativeElement;
    const leftOperand: HTMLInputElement = hostElement.querySelector('#input1');
    const rightOperand: HTMLInputElement = hostElement.querySelector('#input2');
    const result: HTMLSpanElement = hostElement.querySelector('#result');
    const button: HTMLButtonElement = hostElement.querySelector('button');

    fixture.detectChanges();

    leftOperand.value = '3';
    leftOperand.dispatchEvent(new Event('input'));
    rightOperand.value = '4';
    rightOperand.dispatchEvent(new Event('input'));

    button.click();
    fixture.detectChanges();

    expect(result.innerHTML).toEqual('7');
  });
});
